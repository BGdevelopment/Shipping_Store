package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * This class extends the package super class since it needs to have the same
 * variables. This class has two attributes, height and width.
 * 
 * @author bag131
 */
public class Envelope extends Package
{
    private int height;
    private int width;
    
    /**
     * Empty constructor, no need to fill
     */
    public Envelope()
    {
    }

    /**
     * This method just returns the height of the instance
     * @return height
     */
    public int getHeight()
    {
        return height;
    }
    
     /**
     * This method returns the width of the instance.
     * @return width
     */
    public int getWidth() 
    {
        return width;
    }

    /**
     * This method take in a String and parses it to an integer and then
     * sets the instance variable to the integer.  It also gets checked
     * for errors before hand.
     * @param userInput from the keyboard 
     */
    public void setHeight(Scanner userInput)
    {
        String inputFromUser;
        
        do {  
            do {
                System.out.println("Please enter the height of the envelope");
                inputFromUser = userInput.next();
             }while(!InputCorrector.heightChecker(inputFromUser));
        
            this.height = Integer.parseInt(inputFromUser);
        
            System.out.println(this.height + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the height again.");      
        }while(!userInput.next().toLowerCase().equals("y"));
    }

    /**
     * This method takes in a string argument parses it and sets the width of
     * the instance.  If also checks to see if the input was valid.
     * @param userInput from the keyboard
     */
    public void setWidth(Scanner userInput) 
    {
        String inputFromUser;
        
        do {  
            do {
                System.out.println("Please enter the width of the envelope.");
                inputFromUser = userInput.next();
            }while(!InputCorrector.widthChecker(inputFromUser));
        
            this.width = Integer.parseInt(inputFromUser);
            
            System.out.println(this.width + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the width again.");
        }while(!userInput.next().toLowerCase().equals("y")); 
    }
    
    /**
     * This is a TESTING ONLY METHOD.  It just sucks having to constantly type
     * in a new package.  
     * @param fileInput, a string from a file 
     */
    public void setWidth(String fileInput)
    {
        this.width = Integer.parseInt(fileInput);
    }
    
    /**
     * This is a TESTING ONLY METHOD.  It just sucks having to constantly type
     * in a new package.  
     * @param fileInput, a string from a file
     */
    public void setHeight(String fileInput)
    {
        this.width = Integer.parseInt(fileInput);
    }
}