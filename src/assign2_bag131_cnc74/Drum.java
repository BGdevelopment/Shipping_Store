package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * This class is for the drum packages and inherits from the package class.
 * This class has two attributes, material and diameter.
 * @author bag131
 */
public class Drum extends Package
{
    private String material;
    private float diameter;
   
    /**
     * Empty constructor.  No need to fill.
     */
    public Drum()
    {
    }

    /**
     * Simple getter for the material.
     * @return material
     */
    public String getMaterial()
    {
        return material;
    }
    
    /**
     * Simple getter for the diameter.
     * @return diameter
     */
    public float getDiameter()
    {
        return diameter;
    }
    
    /**
     * This method sets the material for the type of drum.  Plastic or Fiber
     * are the valid types and the InputCorrector checks to see if the input
     * is one of those types.
     * @param userInput from the keyboard
     */
    public void setMaterial(Scanner userInput)
    {
        String inputFromUser;
  
        do {    
            do {
                System.out.println("Please enter the volume of the box");
                inputFromUser = userInput.next();
            }while(!InputCorrector.materialChecker(inputFromUser));

            this.material = inputFromUser;
        
            System.out.println(this.material + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the material again.");  
        }while(!userInput.next().toLowerCase().equals("y"));
    }

    /**
     * This method sets the diameter of the drum.  This method asks the user
     * for input and then checks to see if it is a valid float value that is
     * not LTE 0 or a non-float.
     * @param userInput from the keyboard
     */
    public void setDiameter(Scanner userInput)
    {
           String inputFromUser;

        do {  
            do {
                System.out.println("Please enter the volume of the box");
                inputFromUser = userInput.next();
            }while(!InputCorrector.volumeChecker(inputFromUser));
        
            this.diameter = Float.parseFloat(inputFromUser);
        
            System.out.println(this.diameter + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the diameter again."); 
        }while(!userInput.next().toLowerCase().equals("y"));
    }
}