debugging deserialization 

found and fixed bugs: 
- incorrect prompt for user's 9 digit phone number (asked for employee bank account of 10 characters)
- InputCorrector's socialSecurityChecker prints message about bank account not being 9 characters when it's SSN
- InputCorrect reports invalid phone number does not match a mailing class because Customer's
  setPhoneNumber method incorrectly calls it instead of phoneNumberChecker
- InputCorrect's phoneNumberChecker prints message about bank account number
- entering 2222222222 for a customer's phone number throws a numberformatexception, exceeds 32 bit signed int?
  had to use a long and Long.parseLong (exceeded range of parseInt) 
- when trying to print empty packageList, rather than print nothing should inform 
  the user that there are no packages in the database


found bugs, not fixed: 
- input for user phone number, ssn, bank account not forced to be correct length
- formatting for printing transactions could be nicer
---
From Ben: 

I see that I was incorrect on the method I made.  I mean to make it 
modular.  I messed up in that implmentation.  I overloaded the method to take
a string value and return the User without user input now.  Probably too late.

I saw a note about the InputCorrector.idChecker().  Yes it just makes sure
that it's a number that is passed and is greater than zero.  It will return
true if good.

It is just a way of handling the exception and not having massive code in the
method that is being worked on.

Tuesday:
next:
1. get input for and set dates for transactions (user input, validate, set)

3. adjust formatting for printing of transactions and anything else if needed
4. review all files for style
5. make sure javadoc stuff is ok

just done: 
completed transactions now print out. will still need to adjust formatting
remove package from packagelist in completetransaction 

a note: the findUser function can't be used as described - unless we want the user
to enter the information twice - once in the function and once so completeTransaction 
can grab it. this affects deletepackage, etc basically the design is not as modular 
as it could be so a lot of code has to be repeated or worked around.



Tuesday to do:
worked from about 11:45-3pm (skipped class to do this and other homework, hope it was worth it lol) 
got to work on other homework for a little bit, will  be back around 5pm to finish everything
I'll text you before submitting anything 
looks like we'll be finished in time :)








Monday: done:
serialization and deserialization methods have been redone. 
Serialization now saves entire arrayList at once, no need to loop
Deserialization uses a do while loop to read in from the file
exception handling in serialization/deserialization
getter functions in Transaction class
pseudocode for completeTransaction in ShippingDatabase
pseudocode for printTransactionList







----




Hey holmes, I see that there is comments about the output to a file and needing
a customer and employee list.  It's actually easier with just userList.  Instead
of looking for an employee, customer, box, crate, drum, and envelope files, we
just look for user and package files.  If you try to use a new customer and 
employee list then half the code made will not work.  I believe the assignment
was made with the intent for us to use the OOP concept of polymorphism.



The only list that is needing code to be made for is the transaction list
I have made all the code for casting users and packages to their respective 
types.  Searching is done in the user and package lists.  

Below is a proposed change.

     fileName = "User.ser"; 
      ObjectOutputStream os1 = new ObjectOutputStream(new FileOutputStream(fileName)) ;
      try {
    
        // again here there is currently no customerList so this will cause problems
        // need to create customerList
    
        for(User i : userList){  
          os1.writeObject(i); 
          userList.remove(i);  <- I believe this is unnessecary since the list will be written out when the user exits the program.
        }
        os1.close();
      }catch(Exception e){
        System.out.println("Exception: File not found");
        e.printStackTrace();
      }


To do list: 

3. transactions (build transaction class - fill in methods)
4. printing out: adjust formatting 
5. tests (see pushed test methods) 
6. make sure code adheres to bluej style guide
7. verify items 8 and 9 on assignment handout are done 
   a) Complete a shipping transaction.
   b) Show completed shipping transaction
