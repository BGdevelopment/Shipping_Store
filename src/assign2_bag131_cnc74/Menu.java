package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * This class is used to create Menu objects that will display menus.
 * 
 * @author bag131
 * @version 1.0.0
 */
public class Menu 
{       
    /**
     * Empty constructor
     */
    Menu()
    {
    }
    
     /**
      * This method shows the menu that is handled by the main method.
      * The menu looks like :
      * 1. Show all existing package records in the database (in any order).
      * 2. Add new package record to the database.
      * 3. Delete package record from a database.
      * 4. Search for a package (given its tracking number).
      * 5. Show a list of packages within a given weight range.
      * 6. Exit program.
      * 
      */                                   
     public static void showMainMenu()
     {      
         System.out.println("\n1. Show all existing package records in the database .\n" +
                            "2. Add new package record to the database.\n" +
                            "3. Delete package record from a database.\n" +
                            "4. Search for a package (given its tracking number).\n" +
                            "5. Show a list of users in the database. \n" +
                            "6. Add new user to the database. \n" +
                            "7. Update user info (given their id). \n" +
                            "8. Complete a shipping transaction. \n" +
                            "9. Show completed shipping transactions. \n" +
                            "10. Exit program.\n\n"); 
     }              
     
     /**
      * This method calls showMenu() which shows the menu and then gets userinput.
      * The user input is then checked for errors and if valid sent back to the main
      * method to find the correct method to call.
      * @param userInput from the keyboard
      * @return A boolean if valid or not
      */
     public static String mainMenuSelection(Scanner userInput)
     {
         String result;
         
         do {
             showMainMenu();
             
             System.out.println("Please select a number between 1-10.");
             
             result = userInput.next();
         }while(!InputCorrector.mainMenuChecker(result));
         
         return result;
     }
     
     /**
      * Prints the addPackage() menu
      */
    public static void showAddPackageMenu()
    {
        System.out.println("\nPlease select the type of package to add.\n\n"+
                             "1. Envelope\n" +
                            "2. Box\n" +
                            "3. Crate\n" +
                            "4. Drum\n" +
                            "5. Exit\n"
                           );
    }
    
    /**
     * This method gets the result of the selection from the addPackage menu.
     * @param userInput from the keyboard
     * @return the user's selection from the package menu as a string
     */
    public static String addPackageSelection(Scanner userInput)
    {    
        String result;
         
        do {
             showAddPackageMenu();
             
             System.out.println("Please select a number between 1-5.");
             
             result = userInput.next();
         }while(!InputCorrector.addPackageMenuChecker(result));
         
         return result;
    }
    
    /**
     * Creates a menu for the addUser method in the shippingDatabase class.
     */
    public static void userMenu()
    {
         System.out.println("\nPlease select the type of user.\n\n"+
                            "1. Employee\n" +
                            "2. Customer\n" +
                            "3. Exit\n"
                           );
    }
    
    /**
     * This method allows the user to input a number between 1 and 3 and choose
     * between an employee or customer.
     * @param userInput from the keyboard
     * @return a string for the user's selection 
     */
    public static String userSelection(Scanner userInput)
    {
        String result;
         
        do {
             userMenu();
             
             System.out.println("Please select a number between 1-3.");
             
             result = userInput.next();
         }while(!InputCorrector.userMenuChecker(result));
         
         return result;
    } 
}
