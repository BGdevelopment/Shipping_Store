package assign2_bag131_cnc74;
import java.util.*;

/**
 * An instance of this class only becomes populated when the completeTransaction method is called.  Which is called when the user hits the number 8 at the menu screen.
 *
 * @author cnc74
 * @version 1.0.0
 */
public class Transaction
{
    /**
     * Empty constructor. No need to fill in
     */
    public Transaction()
    {
    }

    private int customerID;
    private String packageTrackingNumber;
    private Date shipDate;
    private Date deliveryDate;
    private float shippingCost;
    private int employeeID;
   
    /**
     * Simple getter for the customer id.
     * @return the id number as an integer
     */
    public int getCustomerID()
    {
        return customerID;
    }
    
    /**
     * Simple getter for the tracking number.
     * @return the tracking number as an integer
     */
    public String getTrackingNumber()
    {
        return packageTrackingNumber;
    }

    /**
     * Simple getter for the shipping date.
     * @return the shipping date as a Date
     */
    public Date getShipDate()
    {
        return shipDate;
    }
    
    /**
     * Simple getter for the delivery date.
     * @return the delivery date as a Date
     */
    public Date getDeliveryDate()
    {
        return deliveryDate;
    }
    /**
     * Simple getter for the shipping cost.
     * @return the shipping cost as a float
     */
    public float getShippingCost()
    {
        return shippingCost;
    }
     /**
     * Simple getter for the employee id.
     * @return the employee id as an int
     */
    public int getEmployeeID()
    {
        return employeeID;
    }
    
    /**
     * Simple setter for the customer id
     * @param custID an integer
     */
    public void setCustomerID(int custID)
    {
        this.customerID = custID;
    }
    
    /**
     * Simple setter for the tracking number
     * @param trackingNum a string
     */
    public void setPackageTrackingNumber(String trackingNum)
    {
        this.packageTrackingNumber = trackingNum;
    }
    
    /**
     * Simple setter for the ship date  
     * @param shippingDate, a Date
     */
    public void setShipDate(Date shippingDate)
    {
        this.shipDate = shippingDate;
    }
    
    /**
     * Simple setter for the delivery date  
     * @param delDate, a date
     */
    public void setDeliveryDate(Date delDate)
    {
        this.deliveryDate = delDate;
    }
    
    /**
     * Simple setter for the shipping cost  
     * @param shipCost, a float
     */
    public void setShippingCost(float shipCost)
    {
        this.shippingCost = shipCost;
    }
    
    /**
     * Simple setter for the employee ID  
     * @param empID, an integer
     */
    public void setEmployeeID(int empID)
    {
        this.employeeID = empID;
    }
}