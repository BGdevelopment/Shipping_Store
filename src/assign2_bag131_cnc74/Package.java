package assign2_bag131_cnc74;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * This class creates packages with a tracking number, type, specifications,
 * mailing class, weight, and volume.  The instance's made with this class have
 * their attributes handled by userinput or by file.
 * 
 * @author bag131
 * @version 2.0.0
 */
public class Package implements Serializable
{
    //This variable tells the PackageDatabase class how long to loop
    // to store all attributes from files in a Package.
    public final static int NUMBEROFATTRIBUTES = 6;
    
    //This variable tells the InputCorrector how many characters are in
    // a tracking number.
    public final static int LENGTHOFTRACKINGNUMBER = 5;
    
    private String trackingNumber;      //Tracking number (string of length 5)
    private String specsOfPackage;       //Specification (string)
    private String mailingClass;       //Mailing Class (string)
    
    /**
     * Initializes the current instance to null values.
     */
    Package()
    {
        this.trackingNumber = "";
        this.specsOfPackage = "";
        this.mailingClass = "";
    }
    
     /**
      * Getter for tracking number
      * @return The tracking number of the instance
      */
    public String getTrackingNumber() 
    {
        return trackingNumber;   
    }
    
    /**
     * Getter for the specifications
     * @return The specifications of the instance
     */
    public String getSpecsOfPackage() 
    {
        return specsOfPackage;
    }

    /**
     * Getter for the mailing class
     * @return The mailing class of the instance
     */
    public String getMailingClass() 
    {
        return mailingClass;       
    }
    
     /**
     * This method is a setter for the tracking number of a Package instance
     * and it should only be used when reading from a file.
     * @param trackingFromFile, a string
     */
    public void setTrackingNumber(String trackingFromFile)
    {
        this.trackingNumber = trackingFromFile;
    }
    
    /**
     * This method has been overloaded to only take input from the user, 
     * validate it, and then confirm the entry.
     * @param userInput from the keyboard
     */
    public void setTrackingNumber(Scanner userInput)
    {
        String trackingFromUser;
        
        do{ 
            System.out.println("Please enter a 5 character tracking number. \n");
            
            trackingFromUser = userInput.next();
            
            this.trackingNumber = InputCorrector.trackerChecker(userInput, trackingFromUser);
                             
            System.out.println(this.trackingNumber + " is what is entered," +
                                " is this acceptable? Press Y on the" +
                                " keyboard if so any other key will have you" +
                                " input the tracking number again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }
    

 
    
    /**
     * This method is a normal setter and will set the specifications of a 
     * Package instance, but this method is intended only to be used when
     * reading from a file.
     * @param specsFromFile, a string
     */
    public void setSpecsOfPackage(String specsFromFile) 
    {
        this.specsOfPackage = specsFromFile;
    }
    
    /**
     * This method is an overloaded setter for specs.  This method takes
     * a Scanner instance as an argument, then checks user input, and then 
     * sets the Package's specifications if valid. It also asks the user
     * if they are sure of their input.
     * @param userInput from the keyboard
     */
    public void setSpecsOfPackage(Scanner userInput) 
    {
        do {   
            do {
                System.out.println("Please enter specifications of the type of package." + 
                                " The following are valid types to choose" +
                                " from: Fragile, Books, Catalogs," +
                                " Do-not-Bend, and N/A. \n");
            
                this.specsOfPackage = userInput.next();
             } while(!InputCorrector.specChecker(this.specsOfPackage));
            
            System.out.println(this.specsOfPackage + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the specifications again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }
    
    /**
     * This method is a normal setter method that takes in a String that has
     * a mailing class defined and sets the mailing class of the Package 
     * instance.  This method should only be used when reading from a file.
     * @param mailingClassFromFile, a string
     */
    public void setMailingClass(String mailingClassFromFile) 
    { 
        this.mailingClass = mailingClassFromFile;
    }

    /**
     * This is an overloaded method of the original setter.  This method takes
     * the user's input and sets the mailing class of the Package instance. 
     * 
     * @param userInput from the keyboard
     */
    public void setMailingClass(Scanner userInput) 
    { 
        do{  
            do {
            System.out.println("Please enter the mailing class of the package." + 
                                " The following are valid types to choose" +
                                " from: First-Class, Priority, Retail, Ground," +
                                " Metro \n");
            
            this.mailingClass = userInput.next();
        }while(!InputCorrector.mailingClassChecker(this.mailingClass));
            
            System.out.println(this.mailingClass + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the mailing class again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }
}
