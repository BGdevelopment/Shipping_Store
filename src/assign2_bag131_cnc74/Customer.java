package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * This class inherits from the user class.  It has two extra attributes, 
 * phone number and address.  The address can vary wildly because it is an
 * arbitrary construct.  The phone number will be 10 digits long.
 * 
 * @author bag131
 */
public class Customer extends User
{
    private String phoneNumber;
    private String address;
    
    /**
     * Empty constructor.  No need to fill.
     */
    public Customer()
    {
    }
    
    /**
     * Simple getter for the phone number.
     * @return phoneNumber
     */
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    
    /**
     * Simple getter for the address.  This could be crazy wrong, but not checks
     * can be done on the address since it is too arbitrary.
     * @return address
     */
    public String getAddress()
    {
        return address;
    }
    
    /**
     * This method checks the validity of the phone number given through user
     * input.  The number must be an integer value and must be 10 characters 
     * long starting with the area code.  No other characters besides
     * digits will be accepted.
     * @param userInput from the keyboard
     */
    public void setPhoneNumber(Scanner userInput)
    {
        String inputFromUser;

        do{      
            do {
               System.out.println("Please enter the phone number number of " +
                                  "the customer.  The entry must be 10 characters long." +
                                  "Please enter only numbers starting with area code.\n");

               inputFromUser = userInput.next();
           }while(!InputCorrector.phoneNumberChecker(inputFromUser));

           this.phoneNumber = inputFromUser;
        
           System.out.println(this.phoneNumber + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the mailing class again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }

    /**
     * This is a simple setter.  No check needed since it is too arbitrary.
     * @param userInput from the keyboard
     */
    public void setAddress(Scanner userInput) 
    {
        System.out.println("Please enter customer address.");
        this.address = userInput.next();
    }   
}