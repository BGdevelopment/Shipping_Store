package assign2_bag131_cnc74;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileInputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;

/**
 * Class definition for ShippingDatabase. This class maintains
 * the arraylists for users, packages, and transactions and contain
 * methods that act upon these lists and/or the components of these lists. 
 * It also handles serialization and deserialization of the packages and users
 * @author bag131
 * @author cnc74
 */
public class ShippingDatabase
{
    private ArrayList<Package> packageList = new ArrayList();
    private ArrayList<User> userList = new ArrayList();
    private ArrayList<Transaction> transactionList = new ArrayList();
    
    /**
     * Default constructor, no need to fill in
     */
    public ShippingDatabase()
    {
    }
    
    /**
     * This function prompts the user for input: an employee ID, a customer ID, a tracking number,
     * a shipping cost, a shipping date, and a delivery date. Input is validated before
     * creating a transaction object and removing the package from the packageList.
     * 
     * @param userInput from the keyboard
     */
    public void completeTransaction(Scanner userInput)
    {
        String trackingNumber = null;
        String inputFromUser = null;
        Transaction t = new Transaction();
        boolean idIsValid = false;
        boolean userFound = false;
        
        System.out.println("Creating complete transaction record.");
    
        do {
            try { 
                System.out.println("Enter the employee's ID:");
                inputFromUser = userInput.next();
            }catch(Exception e) {
                    System.out.println("Exception occurred, please try again");
                    inputFromUser = null;
            }
           
            // now check if the ID is in the correct format
            if((idIsValid = InputCorrector.idChecker(inputFromUser))!= false)
            { 
                // it's in the right format, so search to see if user exists
                userFound = findUser(inputFromUser);
                if (userFound == false)
                {
                System.out.println("Could not find user. Try again.");
                }
            }
        }while(!idIsValid || !userFound);
           
        t.setEmployeeID(Integer.parseInt(inputFromUser));
        
        idIsValid = false; 
        userFound = false; 
        
        do {
            try { 
                System.out.println("Enter the customer's ID:");
                inputFromUser = userInput.next();
            }catch(Exception e) {
                    System.out.println("Exception occurred, please try again");
                    inputFromUser = null;
            }
           
            // now check if the ID is in the correct format
            if((idIsValid = InputCorrector.idChecker(inputFromUser))!= false)
            { 
                // it's in the right format, so search to see if user exists
                userFound = findUser(inputFromUser);
                if (userFound == false)
                {
                System.out.println("Could not find user. Try again.");
                }
            }
        }while(!idIsValid || !userFound);
        
        t.setCustomerID(Integer.parseInt(inputFromUser));
        
        boolean isEqual = false;
        do {
            boolean trackNumIsEntered = false; 
            do {
                try {
                    System.out.println("Enter the tracking number:");
                    trackingNumber = userInput.next();
                    trackNumIsEntered = true;
                }catch (Exception e) {
                    System.out.println("Something went wrong. Try again.");
                }
            }while(!trackNumIsEntered);
            
            if (trackingNumber != null)
            {
                for(int i = 0; i < packageList.size(); i++) {
                    isEqual = packageList.get(i).getTrackingNumber().toLowerCase().equals(trackingNumber.toLowerCase());
                    if(isEqual) {
                        t.setPackageTrackingNumber(trackingNumber);   
                        break;
                    }
                }
                if (!isEqual)
                {
                    System.out.println("Package not found. Please try again.");
                }
            }
        }while(!isEqual);
        
        String pattern = "yyyy-MM-dd";
        DateFormat format = new SimpleDateFormat(pattern);
        System.out.println("Please enter the shipping date, with the format yyyy-MM-dd");
        System.out.println("For example, it's now " + format.format(new Date()));
        Date date = null;
        while (date == null) {
            String line = userInput.nextLine();
            line = userInput.nextLine();
            try {
               date = format.parse(line);
            } catch (ParseException e) {
                System.out.println("Sorry, that's not valid. Please try again.");
            }
        }
      
        t.setShipDate(date);
        
        String deliveryPattern = "yyyy-MM-dd";
        DateFormat deliveryFormat = new SimpleDateFormat(deliveryPattern);
        System.out.println("Please enter a delivery date, with the format yyyy-MM-dd");
        System.out.println("For example, it's now " + format.format(new Date()));
        Date deliverDate = null;
        while (deliverDate == null) {
            String line = userInput.nextLine();
            try {
               deliverDate = format.parse(line);
            } catch (ParseException e) {
                System.out.println("Sorry, that's not valid. Please try again.");
            }
        }

        t.setDeliveryDate(deliverDate);
        boolean dateEntered = false;
        
        do {
            try {
                System.out.println("Enter the shipping cost:");
                inputFromUser = userInput.next();
                dateEntered = true;
            }catch(Exception e)
            {
                System.out.println("An exception has occurred, your entry is "
                    + "invalid Try again.");    
            }
        } while(!dateEntered);

        t.setShippingCost(Float.parseFloat(inputFromUser));
                
        transactionList.add(t);
          
        // finally, find and remove the package from package list
        // we have already verified the package is in the list
        boolean equals;
        if (trackingNumber != null)
        {
            for(int i = 0; i < packageList.size(); i++) {
                equals = packageList.get(i).getTrackingNumber().toLowerCase().equals(trackingNumber.toLowerCase());
                if(equals) {
                    packageList.remove(i);         
                    break;
                } 
            }
       }
    }
    
    /**
     * Prints the transactionList to the screen, ie, shows all completed shipping
     * transactions
     */
    public void printTransactionList() {
        String formattedText;
        
        if(!transactionList.isEmpty()) {
            String heading = "Employee ID | Tracking num | Customer ID |  Shipping Cost  |   Shipping Date   |     Delivery Date";
            System.out.println(heading);
        
            for (Transaction i: transactionList) {
                // print transaction info formatted as nicely as possible (work on this)
                formattedText =  String.format("|%1$10s |%2$12s |  %3$10ss   |%4$10s |%5$8s |%6$12s|",
                          i.getEmployeeID(), i.getTrackingNumber(), i.getCustomerID(),
                            i.getShippingCost(), i.getShipDate(), i.getDeliveryDate());
   
                System.out.println(formattedText);  
            } 
        }
        else {
            System.out.println("There are no completed transactions to print.");
        }
    }
    
     /**
     * Prints the userList and packageList to appropriate files
     */
    public void printToFile ()
    {
        System.out.println("Do not close the program yet..." +
                    "printing database to file");
        // save the list of users
        String fileName = "users.ser";
        
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(userList);
              os.close();
        } catch(Exception e) {
          System.out.println("Exception: File not found");
        } 
        // save the list of packages
        fileName = "packages.ser";
        try {
            ObjectOutputStream os2 = new ObjectOutputStream(new FileOutputStream(fileName));
            os2.writeObject(packageList);
            os2.close();
        } catch(Exception e) {
          System.out.println("Exception: File not found");
        } 
        
        System.out.println("It is now safe to close the program. Thank you");
    }
    
     /**
     * Reads the userList and packageList from appropriate files
     */
    public void readFromFile() 
    {   
        System.out.println("Reading user database from file");
        String fileName = "users.ser"; 
        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(fileName)); 
            ArrayList<User> readUserList;
            
            readUserList = (ArrayList<User>) is.readObject();
            if(!readUserList.isEmpty()) {
                userList = readUserList;
                System.out.println(userList.size() + " users loaded");
            } 
            else {
                System.out.println("User list was empty");
            }
            is.close();
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException occurred");
            System.out.println("users.ser was not found");
        }catch(Exception e) {
            System.out.println("Exception occurred reading from users.ser");
        }
      
        System.out.println("Reading package database from file");
        fileName = "packages.ser"; 
        try {
        ObjectInputStream is2 = new ObjectInputStream(new FileInputStream(fileName)); 
        ArrayList<Package> readPackageList;
        
        readPackageList = (ArrayList<Package>) is2.readObject();
        if(!readPackageList.isEmpty()) {
            packageList = readPackageList;
            System.out.println(packageList.size() + " packages loaded");
        } 
        else {
            System.out.println("Package list was empty");
        }
        is2.close();
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException occurred");
            System.out.println("users.ser was not found");
        }catch(Exception e) {
            System.out.println("Exception occurred reading from users.ser");
        }
    }

        /**
     * This method adds a package to the current database instance via user 
     * input.  This creates a menu and adds the correct package based on the
     * menu selection.
     * @param userInput from the keyboard 
     */
    public void addPackage(Scanner userInput)
    {
        String inputAtPackageMenu;
        
         do {
           inputAtPackageMenu = Menu.addPackageSelection(userInput);
           
            switch(inputAtPackageMenu) {       
                //1. Adds an Envelope.
                case "1": Envelope newEnvelope = new Envelope();
                          newEnvelope.setTrackingNumber(userInput);
                          newEnvelope.setHeight(userInput);
                          newEnvelope.setSpecsOfPackage(userInput);
                          newEnvelope.setMailingClass(userInput);
                          newEnvelope.setWidth(userInput);
                          this.packageList.add(newEnvelope);
                          break;
                          
                //2. Adds a Box.
                case "2": Box newBox = new Box();
                          newBox.setTrackingNumber(userInput);
                          newBox.setMaxDimension(userInput);
                          newBox.setSpecsOfPackage(userInput);
                          newBox.setMailingClass(userInput);
                          newBox.setVolume(userInput);
                          this.packageList.add(newBox);
                          break;

                //3. Adds a Crate.
                case "3": Crate newCrate = new Crate();
                          newCrate.setTrackingNumber(userInput);
                          newCrate.setContent(userInput);
                          newCrate.setSpecsOfPackage(userInput);
                          newCrate.setMailingClass(userInput);
                          newCrate.setMaxLoad(userInput);  
                          this.packageList.add(newCrate);      
                          break;

                //4. Adds a Drum.
                case "4": Drum newDrum = new Drum();
                          newDrum.setTrackingNumber(userInput);
                          newDrum.setDiameter(userInput);
                          newDrum.setSpecsOfPackage(userInput);
                          newDrum.setMailingClass(userInput);
                          newDrum.setMaterial(userInput);           
                          this.packageList.add(newDrum);      
                          break;

                //5. Exits the menu
                case "5": System.out.println("Exiting!");
                          break;
            }  
       }while(!inputAtPackageMenu.equals("5"));
    }
 
    /**
     * This method takes a scanner as an argument and asks the user to input 
     * the tracking number of the package to delete from the current instance.
     * @param userInput from the keyboard 
     */
    public void deletePackage(Scanner userInput)
    {
        String trackingNumberToDelete;
        boolean equals;

        do {
            System.out.println("Please enter the tracking number of the package" +
                               " you wish to delete.");
 
                trackingNumberToDelete = userInput.next();
                trackingNumberToDelete = InputCorrector.trackerChecker(userInput, trackingNumberToDelete);

                System.out.println(trackingNumberToDelete + " is what is entered," +
                                    " is this acceptable? Press Y on the" +
                                    " keyboard if so. Any other key will have you" +
                                    " input the tracking number again.");
        }while(!userInput.next().toLowerCase().equals("y"));
        
        for(int i = 0; i < packageList.size(); i++) {
            equals = packageList.get(i).getTrackingNumber().toLowerCase().equals(trackingNumberToDelete.toLowerCase());
            
            if(equals) {
                packageList.remove(i);
                break;
            }
        }
    }
    
    /**
     * This method prints out to the screen the package list given as an 
     * argument.  This design was chosen over using the package list in the
     * instance.  The reason is because if future implementations should be done
     * and printing multiple files, this method can be used to iterate and print
     * any package list.
     * @param listToShow an ArrayList of packages
     */
    public void showPackageList(ArrayList<Package> listToShow)
    {
        String formattedText;
        ArrayList<Envelope> tempEnvelope = new ArrayList();
        ArrayList<Box> tempBox = new ArrayList();
        ArrayList<Crate> tempCrate = new ArrayList();
        ArrayList<Drum> tempDrum = new ArrayList();
        
        ArrayList<Package> sortedList = alphaNumericSort(listToShow);
        
        if (packageList.isEmpty())
        {
            System.out.println("There are not package currently in the database.");
        }
        
        for(Package i : sortedList) {
            
            if(i.getClass() == Envelope.class) {
                tempEnvelope.add((Envelope)i);
            }
            
            else if(i.getClass() == Box.class) {
                tempBox.add((Box)i);
            }
                
            else if(i.getClass() == Crate.class) {
                tempCrate.add((Crate)i);
            }
                    
            else if(i.getClass() == Drum.class) {
                tempDrum.add((Drum)i);
            }
        }
        
        printEnvelopeHeader();
        
        for(Envelope j : tempEnvelope) {
            formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getHeight(),
                          j.getMailingClass(), j.getWidth());
         
            System.out.println(formattedText);
         }
        
        printLine();
         
        printBoxHeader();
        
        for(Box j : tempBox) {
          formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getMaxDimension(),
                          j.getMailingClass(), j.getVolume());
          
          System.out.println(formattedText);
        }
        
        printLine();
          
        printCrateHeader();
         
        for(Crate j : tempCrate){
           formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getMaxLoad(),
                          j.getMailingClass(), j.getContent());
           
           System.out.println(formattedText);
         }
         
         printLine();
         printDrumHeader();
         
         for(Drum j : tempDrum) {
               formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getDiameter(),
                          j.getMailingClass(), j.getMaterial());

                System.out.println(formattedText);
         }
         
         printLine();
    }
    
    /**
     * This method prints out to the screen the current database of packages
     * from the package list belonging to the instance.
     * 
     */
    public void showCurrentPackageDatabase()
    {
        String formattedText;
        ArrayList<Envelope> tempEnvelope = new ArrayList();
        ArrayList<Box> tempBox = new ArrayList();
        ArrayList<Crate> tempCrate = new ArrayList();
        ArrayList<Drum> tempDrum = new ArrayList();
        
        //needs alphaNumericSort
        ArrayList<Package> sortedList = alphaNumericSort(this.packageList);
      
        for(Package i : sortedList) {
            if(i.getClass() == Envelope.class) {
                tempEnvelope.add((Envelope)i);
            }
            
            else if(i.getClass() == Box.class) {   
                tempBox.add((Box)i);   
            }
                
            else if(i.getClass() == Crate.class) {
                tempCrate.add((Crate)i);
            }
                    
            else if(i.getClass() == Drum.class) {
                tempDrum.add((Drum)i);
            }
        }
        
        if(!tempEnvelope.isEmpty()) {
            printEnvelopeHeader();

            for(Envelope j : tempEnvelope) {
                formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                              j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getHeight(),
                              j.getMailingClass(), j.getWidth());

                System.out.println(formattedText);
            }
            printLine();
        }
         
        if(!tempBox.isEmpty()) {     
            printBoxHeader();

            for(Box j : tempBox) {
                formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                              j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getMaxDimension(),
                              j.getMailingClass(), j.getVolume());

                System.out.println(formattedText);
            }

            printLine();
        }
          
        if(!tempCrate.isEmpty()) {
            printCrateHeader();

            for(Crate j : tempCrate) {
              formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                             j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getMaxLoad(),
                             j.getMailingClass(), j.getContent());

              System.out.println(formattedText);
            }
            printLine();
        }
         
        if(!tempDrum.isEmpty()) {
            printDrumHeader();

            for(Drum j : tempDrum) {
                  formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                             j.getTrackingNumber(),  j.getSpecsOfPackage(), j.getDiameter(),
                             j.getMailingClass(), j.getMaterial());

                   System.out.println(formattedText);
            }

            printLine();
        }   
    }
    
    /**
     * This method prints both the employees and the customers to the screen.
     */
    public void showCurrentUserDatabase()
    {
        String formattedText;
        ArrayList<Employee> tempEmployee = new ArrayList();
        ArrayList<Customer> tempCustomer = new ArrayList();
            
        for(User i : this.userList) {
            
            if(i.getClass() == Employee.class) {
                tempEmployee.add((Employee)i);
            }
            
            else if(i.getClass() == Customer.class) {
                tempCustomer.add((Customer)i);     
            }   
        }
        
        if(!tempEmployee.isEmpty()) {
            printEmployeeHeader();

            for(Employee j : tempEmployee) {
                formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s| %6$7s |",
                              j.getFirstName(),  j.getLastName(), j.getId(),
                              j.getMonthlySalary(), j.getBankAccountNumber(),
                              j.getSocialSecurityNumber());

                System.out.println(formattedText);
             }
            printLine();
        }
         
        if(!tempCustomer.isEmpty()) {
            printCustomerHeader();

            for(Customer j : tempCustomer) {
                formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                              j.getFirstName(),  j.getLastName(), j.getId(),
                              j.getPhoneNumber(), j.getAddress());

                System.out.println(formattedText);
            }
            printLine();
        }  
    }
    
    /**
     * This method adds a user to the database.
     * @param userInput from the keyboard 
     */
    public void addUser(Scanner userInput)
    {
         String inputAtUserMenu;
        
         do {
           inputAtUserMenu = Menu.userSelection(userInput);
           
            switch(inputAtUserMenu) {       
                // 1. Adds an Emoployee
                case "1": Employee newEmployee = new Employee();
                          newEmployee.setFirstName(userInput);
                          newEmployee.setLastName(userInput);    
                          newEmployee.setId(userInput);
                          newEmployee.setBankAccountNumber(userInput);
                          newEmployee.setMonthlySalary(userInput);
                          newEmployee.setSocialSecurityNumber(userInput);    
                          this.userList.add(newEmployee);       
                          break;

                // 2. Adds a Customer.
                case "2": Customer newCustomer = new Customer();
                          newCustomer.setFirstName(userInput);
                          newCustomer.setLastName(userInput);
                          newCustomer.setId(userInput);
                          newCustomer.setPhoneNumber(userInput);
                          newCustomer.setAddress(userInput);     
                          this.userList.add(newCustomer);       
                          break;

                // 3. Quits.
                case "3": System.out.println("Exiting!");
                          break;
            }
       }while(!inputAtUserMenu.equals("3"));
    }
       
    /**
     * This method returns the user in the already loaded database of the
     * ShippindDatabase instance.  
     * @param userInput from the keyboard
     * @return the package found in the current database instance
     */
    public User findUser(Scanner userInput)
    {
        String idNumber;
        
        do{
            System.out.println("Please input an ID number to find.");

            idNumber = userInput.next();

             for(User i : this.userList) {
                if(i.getId().toString().equals(idNumber)) {
                    return i;
                }
            }
        }while(!InputCorrector.idChecker(idNumber));
        
        System.out.println("Could not find user.  Returning null");
        
        return null;
    }
    
    public boolean findUser(String userInput)
    {
 
        do{
             for(User i : this.userList) {
                if(i.getId().toString().equals(userInput)) {
                    return true;
                }
            }
        }while(!InputCorrector.idChecker(userInput));
        
        return false;
    }
    
    
    /**
     * This method uses the findUser method and then casts the user found in
     * to the correct type and then forces the user to input the values of the 
     * User all over again.
     * @param userInput from the keyboard 
     */
    public void updateUserInfo(Scanner userInput)
    {
        String inputAtUserMenu;
        
         do {
           inputAtUserMenu = Menu.userSelection(userInput);
           
            switch(inputAtUserMenu) {       
                //1. Updates an employee
                case "1": Employee newEmployee = (Employee)findUser(userInput);
                          newEmployee.setFirstName(userInput);
                          newEmployee.setLastName(userInput);
                          newEmployee.setBankAccountNumber(userInput);
                          newEmployee.setMonthlySalary(userInput);
                          newEmployee.setSocialSecurityNumber(userInput);              
                          break;

                //2. Updates a customer
                case "2": Customer newCustomer =  (Customer)findUser(userInput);
                          newCustomer.setFirstName(userInput);
                          newCustomer.setLastName(userInput);
                          newCustomer.setId(userInput);
                          newCustomer.setPhoneNumber(userInput);
                          newCustomer.setAddress(userInput);
                          break;
                //3. Quits.
                case "3": System.out.println("Exiting!");
                          break;
            }
       }while(!inputAtUserMenu.equals("3"));
    }
    
    /**
     * This method will show a package that has been found.  It requires
     * a Package instance as an argument to print correctly.
     * @param packageToShow a package
     */
    public void showPackage(Package packageToShow)
    {
        String formattedText;
        
        if(packageToShow.getClass() == Envelope.class) {
            printEnvelopeHeader();
            
            Envelope temp = (Envelope)packageToShow;
            
            formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          temp.getTrackingNumber(),  temp.getSpecsOfPackage(), temp.getHeight(),
                          temp.getMailingClass(), temp.getWidth());
         
            System.out.println(formattedText);
            printLine();
         }

        else if(packageToShow.getClass() == Box.class) {
            printBoxHeader();
            
            Box temp = (Box)packageToShow;
            
            formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          temp.getTrackingNumber(),  temp.getSpecsOfPackage(), temp.getMaxDimension(),
                          temp.getMailingClass(), temp.getVolume());
          
            System.out.println(formattedText);
            printLine();
        }
         
        else if(packageToShow.getClass() == Crate.class) {
            printCrateHeader();
            
            Crate temp = (Crate)packageToShow;
            
            formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          temp.getTrackingNumber(),  temp.getSpecsOfPackage(), temp.getMaxLoad(),
                          temp.getMailingClass(), temp.getContent());
           
           System.out.println(formattedText);
           printLine();
         }
         
        else if(packageToShow.getClass() == Drum.class) {
            printDrumHeader();
            
            Drum temp = (Drum)packageToShow;
            
            formattedText =  String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s|",
                          temp.getTrackingNumber(),  temp.getSpecsOfPackage(), temp.getDiameter(),
                          temp.getMailingClass(), temp.getMaterial());

            System.out.println(formattedText);
            printLine();
         }
        
        else {
            System.out.println("The package could not be shown.  This is probably" +
                                " because the classes are not matching.");
        }
    }
 
    
    /**
     * This method returns the package in the already loaded database of the
     * ShippindDatabase instance.  
     * @param userInput from the keyboard
     * @return the package found in the current database instance
     */
    public Package findPackage(Scanner userInput)
    {
        String finalizedTrackingNumber;
        
        System.out.println("Please input a tracking number to find.");
        
        finalizedTrackingNumber = InputCorrector.trackerChecker(userInput, userInput.next());
        
         for(Package i : this.packageList) {
            if(i.getTrackingNumber().equals(finalizedTrackingNumber)) {
                return i;
            }
        }
        
        System.out.println("Could not find package.  Returning null");
        
        return null;
    }
    
    /**
     * This method is for testing only!!!!!!!  IT SUCKS TYPING IN A PACKAGE ALL THE TIME.
     */
    public void readPackagesFromFile()
    {
        Scanner inputFile;
        String fileName ="packageInput.txt";

        try {
            inputFile = new Scanner(new FileReader(fileName));

            while(inputFile.hasNext()){
                
                    Envelope newPackage = new Envelope();

                    newPackage.setTrackingNumber(inputFile.next());
                    newPackage.setHeight(inputFile.next());
                    newPackage.setSpecsOfPackage(inputFile.next());
                    newPackage.setMailingClass(inputFile.next());
                    newPackage.setWidth(inputFile.next());

                    
                    packageList.add(newPackage);
            } 
        }catch(FileNotFoundException e) {
             System.out.println(fileName + " not found a file will be created with" + 
                               " your entries upon exit.");
           
            e.getMessage();
        } 
    }
    
    private void printCustomerHeader()
    {       
        printLine();
        
        System.out.println(String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s |",
                                         "FIRSTNAME #", "LASTNAME", "ID", "PHONE", "ADDRESS"));
        
        printLine();
    }
    
    /**
     * This method prints a header to understand what attributes the employee
     * has.
     */
    private void printEmployeeHeader()
    {
        printLine();
        
        System.out.println(String.format("|%1$10s |%2$9s |%3$9s |%4$6s |%5$8s |%6$9s |",
                                         "FIRSTNAME", "LASTNAME", "ID", "SALARY", "BANKACCOUNT",
                                         "SSN"
                                        ));
        
        printLine();
    }
    /**
     * This method prints the header that displays what the packages features
     * are.
     */
    private void printEnvelopeHeader()
    {
        printLine();
        
        System.out.println(String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s |",
                                         "TRACKING #", "TYPE", "HEIGHT", "CLASS", "WIDTH"
                                        ));

        printLine();
    }
    
    private void printBoxHeader()
    {
        printLine();
        
        System.out.println(String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s |",
                                         "TRACKING #", "TYPE", "MAX DIMENSION", "CLASS", "VOLUME"
                                         ));
        
        printLine();
    }
    
    private void printCrateHeader()
    {
        printLine();
        
        System.out.println(String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s |",
                                         "TRACKING #", "TYPE", "MAX LOAD", "CLASS", "CONTENTS"
                                         ));

        printLine();
    }
    
    private void printDrumHeader()
    {
        printLine();
        
        System.out.println(String.format("|%1$10s |%2$9s |%3$13s |%4$10s |%5$8s |",
                                         "TRACKING #", "TYPE", "DIAMETER", "CLASS", "MATERIAL"
                                         ));
        
        printLine();
    }
    
    /**
     * This method is a helper method to printHeader and just produces a line.
     */
    private void printLine()
    {
        System.out.println("----------------------------------------------------------------------");
    }
    
    private ArrayList<Package> alphaNumericSort(ArrayList<Package> toBeSorted){
        
        ArrayList<Package> tempList = new ArrayList();
        tempList = toBeSorted;
        Package temp;
       
        for(int i = 0; i < tempList.size(); i++){
            for(int j = 0; j < tempList.size(); j++){

                if(tempList.get(i).getTrackingNumber().compareTo(tempList.get(j).getTrackingNumber()) <= 0) {

                    temp = tempList.get(i);
                    tempList.set(i, tempList.get(j));
                    tempList.set(j, temp);
                }
            }
        }
        return tempList;
    }
}

