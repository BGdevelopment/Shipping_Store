package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * This is the box package class that inherits from the package class.  This
 * class has two attributes, max dimension and volume.
 * @author bag131
 */
public class Box extends Package
{
    private int maxDimension;
    private int volume;

    /**
     * Empty Constructor.  No need to fill
     */
    public Box()
    {    
    }
    
    /**
     * Simple getter for the max Dimension.
     * @return the maximum dimension as an integer
     */
    public int getMaxDimension()
    {
        return maxDimension;
    }
    
    /**
     * Simple getter for the volume.
     * @return the volume, an integer
     */
    public int getVolume()
    {
        return volume;
    }
    
    /**
     * This setter is an overloaded version of the original which checks user
     * input for an integer and parses it into the volume attribute of the 
     * Package instance.
     * @param userInput from the keyboard 
     */
    public void setVolume(Scanner userInput) 
    {
        String inputFromUser;
          
        do {  
            do {
                System.out.println("Please enter the volume of the box"); 
                inputFromUser = userInput.next();
            }while(!InputCorrector.volumeChecker(inputFromUser));

            this.volume = Integer.parseInt(inputFromUser);
        
            System.out.println(this.volume + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the volume again.");
            
        }while(!userInput.next().toLowerCase().equals("y"));
    }
     
     /**
      * This method sets the max dimension of the box.  It asks the user
      * to input the dimension.  The InputCorrector class checks to see if
      * the entry is valid.  The input can not be LTE 0 or a non-Integer.
      * @param userInput from the keyboard
      */
    public void setMaxDimension(Scanner userInput)
    {
        String inputFromUser;

        do {          
            do {
                System.out.println("Please enter the largest dimension of the box");  
                inputFromUser = userInput.next();

                }while(!InputCorrector.dimensionChecker(inputFromUser));

            this.maxDimension = Integer.parseInt(inputFromUser);
            
            System.out.println(this.maxDimension + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the dimension again.");
            
        }while(!userInput.next().toLowerCase().equals("y"));
    }
}