package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * 
 * This class corrects input from the user or lets the user know they need to
 * correct their input.  This class only contains static methods since it has no
 * attributes.
 * 
 * @author bag131
 * @author cnc74
 * @version 1.0.0
 */
public class InputCorrector 
{
    /**
     * Empty constructor
     */
    InputCorrector()
    {       
    }
    
    /**
     * This method is used to correct the input for a tracking number.  The input
     * will be cut to 5 characters if it has more than 5 characters.  The user
     * will be asked to input a new tracking number if it is too short.  This 
     * method returns the tracking number as a String.
     * @param userInput from the keyboard
     * @param trackingNumber, a string
     * @return a String that has been corrected or that was initially valid
     */
    public static String trackerChecker(Scanner userInput, String trackingNumber)
    {
        String result = trackingNumber;
        
        do {       
            if(result.length() == Package.LENGTHOFTRACKINGNUMBER) {
                result = trackingNumber;
            }
            else if (result.length() < Package.LENGTHOFTRACKINGNUMBER){
                System.out.println("The input given is too short, there must" +
                                   " be 5 characters entered. \nPlease reenter" +
                                   " 5 characters. \n" );

                result = userInput.next();
            }
            else{
                System.out.println("The input is too long. Please shorten " +
                                   "to 5 characters. \n");
                
                result = userInput.next();
            }
        }while(result.length() != 5);
        
        return result;
    }
         
    /**
     * This method checks the type that the user inputs for a package. This method
     * will return a boolean stating that the type is good or let the user 
     * know that the type is bad.
     * @param typeInput, a string
     * @return a boolean if valid or not
     */
    public static boolean typeChecker(String typeInput)
    {
        String[] validTypes = { "Envelope", " Box", "Crate", "Drum" };
        boolean isCorrectType = false;
      
        for(int i = 0; i < validTypes.length; i++) {
            isCorrectType = typeInput.toLowerCase().contains(validTypes[i].toLowerCase());
            
            if(isCorrectType) {
                return true;
            } 
        }
        
        if(!isCorrectType) { 
            System.out.println("The input does not match a valid type." + 
                               " Please reenter a type for the package. \n");    
        }
        
        return isCorrectType;
    }
    
    /**
     * This method checks the specifications of a package and returns a boolean
     * if the specifications are good.  Otherwise, it will let the user know
     * that they need to enter new specifications.
     * 
     * @param specInput, a string
     * @return a boolean if valid or not
     */
    public static boolean specChecker(String specInput)
    {
        String[] validTypes = {"Fragile", "Books", "Catalogs",
                                "Do-not-Bend", "N/A"};
        boolean isCorrectType = false;
      
        for(int i = 0; i < validTypes.length; i++) {
            isCorrectType = specInput.toLowerCase().equals(validTypes[i].toLowerCase());
            
            if(isCorrectType) {
                return true;
            }
        }
        
        if(!isCorrectType) {
            System.out.println("The input does not match a valid specs. " + 
                               "Please enter new specifications for the package. \n");
        }
        return isCorrectType;
    }
    
    /**
     * This method is used to check that the user has put in a valid mailing class
     * for the package.  It returns a boolean if it is good or not and lets
     * the user know the mailing class was not valid if not.
     * @param mailingClassInput, a string
     * @return a boolean if valid or not
     */
    public static boolean mailingClassChecker(String mailingClassInput)
    {
        String[] validTypes = {"First-Class", "Priority", "Retail", "Ground",
                               "Metro"};
        boolean isCorrectType = false;
      
        for(int i = 0; i < validTypes.length; i++) {
            isCorrectType = mailingClassInput.toLowerCase().equals(validTypes[i].toLowerCase());
            
            if(isCorrectType) {
                return true;
            }
        }
        
        if(!isCorrectType) {
            System.out.println("The input does not match a valid mailing class. " + 
                               "Please enter a mailing class for the package. \n");
        }
        
        return isCorrectType;
    }
    
    /**
     * This method is used to check that the user has put in a valid weight for
     * the package.  It returns a boolean if it is good or not and lets
     * the user know the weight was not valid if not.
     * @param weightInput, a string
     * @return a booean if weight is valid or not
     */
    public static boolean weightChecker(String weightInput)
    {
        boolean isGood;
        
        try {
             float result = Float.parseFloat(weightInput);
             isGood = true;
             
             if(isGood && result <= 0) {
                 return true;
             }
             else {
                 return false;
             }
             
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number.  Please input" +
                               " a float number. \n");
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This method is used to check that the user has put in a valid volume for
     * the package.  It returns a boolean if it is good or not and lets
     * the user know the weight was not valid if not.
     * @param volumeInput, a string
     * @return a boolean if valid or not
     */
     public static boolean volumeChecker(String volumeInput)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(volumeInput);
             isGood = true;
             
             if(isGood && result >= 0) {
                return true;
             }
             else {
                 return false;
             }
             
        }catch(NumberFormatException e) {
            
            System.out.println("The input is not a number.  Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
    
     /**
      * This method checks the input at the menu screen and checks if it is an
      * integer and is between 1-10.
      * @param menuInput, a string
      * @return a boolean if valid or not
      */
    public static boolean mainMenuChecker(String menuInput)
    {
        int parsedMenuInput;
        
        try {
            parsedMenuInput = Integer.parseInt(menuInput);
        }catch(NumberFormatException e) {
            System.out.println("Entry is not a valid integer.\n");
            return false;
        }
        
        if(parsedMenuInput <= 10 && parsedMenuInput >= 1) {
            return true;
        }
        else {
            System.out.println("Entry is not between 1-10.\n");
            return false;
        }
    }
    
     /**
      * This method checks the input at the menu screen and checks if it is an
      * integer and is between 1-5.
      * @param menuInput, a string
      * @return a boolean if valid or not
      */
    public static boolean addPackageMenuChecker(String menuInput)
    {
        int parsedMenuInput;
        
        try {
           parsedMenuInput = Integer.parseInt(menuInput);
        }catch(NumberFormatException e) {
            System.out.println("Entry is not a valid integer.\n");
            return false;
        }
        
        if(parsedMenuInput <= 5 && parsedMenuInput >= 1) {
            return true;
        }
        else {
            System.out.println("Entry is not between 1-5.\n");
            return false;
        }
    }
    
    /**
     * This method checks the validity of user input for height in the Envelope
     * class.
     * @param input, a string
     * @return a boolean representing whether the height is valid or not
     */
    public static boolean heightChecker(String input)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(input);
             isGood = true;
             
             if(isGood && result >= 0) {
                return true;
             }
             else{
                 return false;
             }
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number.  Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
    
     /**
     * This method checks the validity of user input for width in the Envelope
     * class.
     * @param input, a string
     * @return a boolean representingn whether the width is valid or not
     */
    public static boolean widthChecker(String input)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(input);
             isGood = true;
             
             if(isGood && result >= 0) {
                return true;
             }
             else{
                 return false;
             }
             
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number.  Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
     /**
     * This method checks the validity of user input for dimension in the Box
     * class.
     * @param input, a string
     * @return a boolean indicating whether the dimensions are valid or not
     */
    public static boolean dimensionChecker(String input)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(input);
             isGood = true;
             
             if(isGood && result >= 0) {
                return true;
             }
             else{
                 return false;
             }
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number.  Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
    
     /**
     * This method checks the validity of user input for material in the Drum
     * class.
     * @param input, a string
     * @return a boolean indicating whether the material is valid or not
     */
    public static boolean materialChecker(String input)
    {
        String[] validTypes = {"Plastic", "Fiber"};
        boolean isCorrectType = false;
      
        for(int i = 0; i < validTypes.length; i++) {
            
            isCorrectType = input.toLowerCase().equals(validTypes[i].toLowerCase());
            
            if(isCorrectType) {
                return true;
            }
        }
        
        if(!isCorrectType){
            
            System.out.println("The input does not match a valid materials." + 
                               "Please enter new specifications for the package. \n");
        }
        
        return isCorrectType;
    }
    
    /**
     * This method checks the validity of user input for diameter in the Drum
     * class.
     * @param input, a string
     * @return a boolean indicating whether the diameter is valid or not
     */
    public static boolean diameterChecker(String input)
    {
        boolean isGood;
        
        try {
             float result = Float.parseFloat(input);
             isGood = true;
             
             if(isGood && result >= 0){
                 return true;
             }
             else{
                 return false;
             }
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number.  Please input" +
                               " a float number. \n");
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This class checks the user input for the ID of a user.
     * @param input, a string
     * @return a boolean indicating whether the id is valid or not
     */
    public static boolean idChecker(String input)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(input);
             isGood = true;
             
             if(isGood && result >= 0) {
                return true;
             }
             else{
                 return false;
             }
             
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number. Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This class checks the user input for the ID of a user.  The string must
     * contain no numbers.
     * @param input, a string
     * @return a boolean indicating whether the name is valid or not
     */
    public static boolean nameChecker(String input)
    {
        
        String[] invalidTypes = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        boolean containsNumber = false;
      
        for(int i = 0; i < invalidTypes.length; i++) {
            containsNumber = input.toLowerCase().contains(invalidTypes[i].toLowerCase());
            
            if(containsNumber) {
                System.out.println("The input contains a number.  Names do not have" +
                               " numbers.");
                return false;
            }
        }
        
        if(!containsNumber) {
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * This method checks the validity of the social security number.  The input
     * must be a string of length 9 and must be an integer.
     * @param input, a string
     * @return a boolean indicating whether the SSN is valid or not
     */
    public static boolean socialSecurityChecker(String input)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(input);
             isGood = true;
             
             if(isGood && result >= 0 && input.length() == 9) {
                return true;
             }
             else{
                 System.out.println("The SSN given is not 9 " +
                                    "characters long.\n");
                 return false;
             }
        }catch(NumberFormatException e){
            System.out.println("The input is not a number. Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This method checks the validity of the monthly salary.
     * @param input, a string
     * @return a boolean indicating whether the monthly salary is valid or not
     */
    public static boolean monthlySalaryChecker(String input)
    {
        boolean isGood;
        
        try {
             float result = Float.parseFloat(input);
             isGood = true;
             
             if(isGood && result >= 0) {
                 return true;
             }
             else{
                 return false;
             }
             
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number.  Please input" +
                               " a float number. \n");
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This method checks the validity of a bank account number.
     * @param input, a string
     * @return a boolean indicating whether the bank account number is valid or not
     */
    public static boolean bankAccountNumberChecker(String input)
    {
        boolean isGood = false;
        
        try{
             int result = Integer.parseInt(input);
             isGood = true;
             
             if(isGood && result >= 0 && input.length() == 9) {
                return true;
             }
             else{
                 System.out.println("The bank account number given is not 9 " +
                                    "characters long.\n");
                 return false;
             }
             
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number. Please input" +
                               " a number. \n");
            
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This method checks the validity of a phone number.
     * @param input, a string
     * @return a boolean indicating whether the phone number is valid or not
     */
    public static boolean phoneNumberChecker(String input)
    {
        boolean isGood = false;
        
        try{
            long result = Long.parseLong(input);
            isGood = true;
            
            if(isGood && result >= 0 && input.length() == 10) {
               return true;
            }
            else{
                System.out.println("The phone number given is not 10 " +
                                    "characters long.\n");
                return false;
            }
        }catch(NumberFormatException e) {
            System.out.println("The input is not a number. Please input" +
                               " a number. \n");
            isGood = false;
        }
        
        return isGood;
    }
    
    /**
     * This method checks the validity of the user input to find what type
     * of user they want.
     * @param menuInput, a string
     * @return a boolean indicating whether the menu choice is valid or not
     */
    public static boolean userMenuChecker(String menuInput)
    {
        int parsedMenuInput;
        
        try {
           parsedMenuInput = Integer.parseInt(menuInput);
        }catch(NumberFormatException e){
            System.out.println("Entry is not a valid integer.\n");
            return false;
        }
        
        if(parsedMenuInput <= 3 && parsedMenuInput >= 1) {
            return true;
        }
        else {
            System.out.println("Entry is not between 1-3.\n");
            return false;
        }
    }
}
