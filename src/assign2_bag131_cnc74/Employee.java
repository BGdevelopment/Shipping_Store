package assign2_bag131_cnc74;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * This class inherits from user and has three extra attributes, social security
 * number, monthly salary, and bank account number.
 * 
 * @author bag131
 */
public class Employee extends User
{
    private int socialSecurityNumber;
    private float monthlySalary;
    private int bankAccountNumber;

    /**
     * Empty constructor.  No need to fill.
     */
    public Employee() 
    {
    }
    
    /**
     * This method is a simple getter for the social security number.
     * @return socialSecurityNumber
     */
    public int getSocialSecurityNumber()
    {
        return socialSecurityNumber;
    }

    /**
     * This method is a simple getter for the monthly salary.
     * @return monthlySalary
     */
    public float getMonthlySalary()
    {
        return monthlySalary;
    }
    
    /**
     * This method is a simple getter for the bank account number.
     * @return bankAccountNumber
     */
    public int getBankAccountNumber()
    {
        return bankAccountNumber;
    }
    
    /**
     * This method sets the social security number of the user by user input.
     * The InputCorrector validates it.
     * @param userInput from the keyboard 
     */
    public void setSocialSecurityNumber(Scanner userInput)
    {
        String inputFromUser;
         
        do{  
             do {
                System.out.println("Please enter the social security number of " +
                                   "the employee.  The entry must be 9 characters long.\n");

                inputFromUser = userInput.next();
        }while(!InputCorrector.socialSecurityChecker(inputFromUser));
        
        this.socialSecurityNumber = Integer.parseInt(inputFromUser);
        
        System.out.println(this.socialSecurityNumber + " is what is entered," +
                            " is this acceptable? Press Y on the" +
                            " keyboard if so any other key will have you" +
                            " input the mailing class again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }

    /**
     * This method sets the monthly salary via user input and is checked for
     * validity by the InputCorrector.  It also becomes formatted.
     * @param userInput from the keyboard 
     */
    public void setMonthlySalary(Scanner userInput)
    {
        String inputFromUser;

        do {  
            do {
                System.out.println("Please enter the monthly salary of the employee");
                inputFromUser = userInput.next();
            }while(!InputCorrector.monthlySalaryChecker(inputFromUser));
        
            this.monthlySalary = Float.parseFloat(inputFromUser);
            this.monthlySalary = Float.parseFloat(new DecimalFormat("##.##").format(this.monthlySalary));

            System.out.println(this.monthlySalary + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the max load again.");
            
        }while(!userInput.next().toLowerCase().equals("y"));
    }

    /**
     * This method sets the bank account number via user input and the 
     * InputCorrector checks the validity of the input.
     * @param userInput from the keyboard 
     */
    public void setBankAccountNumber(Scanner userInput)
    {
        String inputFromUser;
        
        do{  
            do {
                System.out.println("Please enter the bank account number of " +
                                   "the employee.  The entry must be 9 characters long.\n");

                inputFromUser = userInput.next();
             }while(!InputCorrector.bankAccountNumberChecker(inputFromUser));
        
            this.bankAccountNumber = Integer.parseInt(inputFromUser);
        
            System.out.println(this.bankAccountNumber + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the mailing class again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }
}
