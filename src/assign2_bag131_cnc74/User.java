package assign2_bag131_cnc74;
import java.io.Serializable;
import java.util.Scanner;

/**
 * This is the user class, from which Employee and Customer both inherit. This
 * class hass 3 attributes, an ID number, a first name, and a last name
 * @author bag131
 * @version 1.0.0
 */
public class User implements Serializable
{
    private Integer id;
    private String firstName;
    private String lastName;
    
    /**
     * Empty Constructor. No need to fill.
     */
    public User() 
    {
    }
    /**
     * Simple getter for the id.
     * @return the id number as an integer
     */
    public Integer getId()
    {
        return id;
    }
    
    /**
     * Simple getter for the first name.
     * @return the first name as a string
     */
    public String getFirstName()
    {
        return firstName;
    }
   
    /**
     * Simple getter for the last name.
     * @return the last name as a string
     */
    public String getLastName() 
    {
        return lastName;
    }

     /**
     * This setter checks user input for an integer and parses it into the
     * ID attribute
     * @param userInput from the keyboard
     */
    public void setId(Scanner userInput) 
    {
        String inputFromUser;

        do {  
            do {
                System.out.println("Please enter the id of the user");

                inputFromUser = userInput.next();
            }while(!InputCorrector.idChecker(inputFromUser));
        
        this.id = Integer.parseInt(inputFromUser);
        
        System.out.println(this.id + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the volume again.");  
        }while(!userInput.next().toLowerCase().equals("y"));
    }
    
    /**
     * This setter checks user input for a string and parses it into
     * the firstName attribute of a User
     * @param userInput from the keyboard
     */
    public void setFirstName(Scanner userInput)
    {
        do{   
            do {
            System.out.println("Please enter the first name of the user. \n");
            
            this.firstName = userInput.next();  
        }while(!InputCorrector.nameChecker(this.firstName));
            System.out.println(this.firstName + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the mailing class again.");  
        }while(!userInput.next().toLowerCase().equals("y"));
    }
    /**
     * This setter checks user input for a string and parses it into
     * the lastName attribute of a User
     * @param userInput from the keyboard
     */
    public void setLastName(Scanner userInput)
    {
        do{  
            do {
            System.out.println("Please enter the last name class of the user. \n");
            
            this.lastName = userInput.next();
        }while(!InputCorrector.nameChecker(this.lastName));
            
            System.out.println(this.lastName + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the mailing class again.");
        }while(!userInput.next().toLowerCase().equals("y"));
    }
}