package assign2_bag131_cnc74;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * This class is for the crate packages and inherits from the Package class.
 * This class has two attributes, max load and content of the crate.
 * @author bag131
 */
public class Crate extends Package
{
    private float maxLoad;
    private String content;
    
    /**
     * Empty Constructor. No need to fill
     */
    public Crate()
    { 
    }
    
    /**
     * This method is a simple getter for the max load of the instance.
     * @return maxLoad
     */
    public float getMaxLoad()
    {
        return maxLoad;
    }
    
    /**
     * This method is a simple getter for the content of the instance.
     * @return content
     */
    public String getContent()
    {
        return content;
    }
    
    /**
     * This method sets the maximum load that the crate can handle.  This method
     * asks the user to input the max load and checks to see if it is a valid
     * entry.  It can not be 0 and it must be an integer.
     * @param userInput from the keyboard
     */
    public void setMaxLoad(Scanner userInput)
    {
        String inputFromUser;
        do {            
            do {
                System.out.println("Please enter the max load weight of the crate");
                inputFromUser = userInput.next();
            }while(!InputCorrector.weightChecker(inputFromUser));

            this.maxLoad = Float.parseFloat(inputFromUser);
            this.maxLoad = Float.parseFloat(new DecimalFormat("##.##").format(this.maxLoad));
            
            System.out.println(this.maxLoad + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the max load again.");
            
        }while(!userInput.next().toLowerCase().equals("y"));
    }
    
    /**
     * This method sets the content of the crate instance by asking the user
     * for the contents.  There is no need to check if the content is valid
     * since content is arbitrary.  InputChecker is not used in this method.
     * @param userInput from the keyboard
     */
    public void setContent(Scanner userInput)
    {
        do {  
            System.out.println("Please enter the max load weight of the crate");
            this.content = userInput.next();
        
            System.out.println(this.maxLoad + " is what is entered," +
                               " is this acceptable? Press Y on the" +
                               " keyboard if so any other key will have you" +
                               " input the max load again.");
            
        }while(!userInput.next().toLowerCase().equals("y"));
    }
}