/**
 * This program uses the BlueJ style guide.
 */
package assign2_bag131_cnc74;
import java.util.Scanner;

/**
 * This class deals with all package types and user types.  It stores arrayLists
 * of the two super class types.  This class is responsible for displaying the
 * lists, interpreting external lists, saving objects, loading objects, and 
 * formatting the printed displays.
 * 
 * @author bag131
 * @author cnc74
 */
public class Assign2_bag131_cnc74
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
       //String fileName = "packages.txt";
       String inputAtMenu;

       ShippingDatabase database = new ShippingDatabase();
       
       Scanner userInput = new Scanner(System.in);
       
        database.readFromFile();
       //database.readPackagesFromFile();
       
       do { 
           inputAtMenu = Menu.mainMenuSelection(userInput);
           
            switch(inputAtMenu) {       
                // 1. Show all existing package records in the database (in any order).
                case "1": database.showCurrentPackageDatabase();
                        break;

                // 2. Add new package record to the database.
                case "2": database.addPackage(userInput);
                        break;

                // 3. Delete package record from a database.
                case "3": database.deletePackage(userInput);
                        break;

                // 4. Search for a package (given its tracking number).
                case "4": database.showPackage(database.findPackage(userInput));
                        break;

                // 5. Show a list of users, AKA employee and Customers.
                case "5": database.showCurrentUserDatabase();
                        break;

                // 6. Adds a new user to the database.
                case "6": database.addUser(userInput);
                        break;
                          
                // 7. Updates the user info given and ID.      
                case "7": database.updateUserInfo(userInput);
                        break;
                          
                // 8. Complete a shipping transaction.      
                case "8": database.completeTransaction(userInput);
                          break;
                          
                    //9. Shows the completed shipping transactions.      
                case "9": database.printTransactionList();
                          break;
                          
                    //10.  Exit program      
                case "10":
                          break;
            }
            
       }while(!inputAtMenu.equals("10"));
       
       // serialize the database before closing the program
       database.printToFile();
    }
}
